// pages/california.js
module.exports = {
    url: 'http://dev.exsymol.mc:1026',
    elements: {
        usernameField: {
            selector: '#UserUsername'
        },
        passwordField: {
            selector: '#UserPassword'
        }
    },
    commands: [{
        signInAsUser: function(username, password) {
            return this.setValue('@usernameField', username)
                    .setValue('@passwordField', password)
                    .submitForm('form#UserLoginForm')
                    .api.pause(8000);
        }
    }]
}
