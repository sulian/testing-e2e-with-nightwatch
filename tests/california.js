// tests/california.js
global.__base = __dirname + '\\'
auth_creds = require(__base + 'auth_creds.json')

const screenshotsPath = 'reports/california/screenshots/'

module.exports = {
    '@tags': ['login', 'california'],
    'Etape 1 : ouverture de session': function (browser) {
        let page = browser.page.california()

        page.navigate()
            .waitForElementVisible('body', 1000);

        browser.saveScreenshot(screenshotsPath + 'login-form.png');

        browser.expect.element("#UserLoginForm").to.be.present;
        browser.expect.element("#UserUsername").to.be.present;
        browser.expect.element("#UserPassword").to.be.present;

        page.navigate()
            .signInAsUser(auth_creds.username, auth_creds.password)
            .waitForElementVisible('.main', 1000);
    },
    'Etape 2 : vérification des applications disponibles': function (browser) {
        let page = browser.page.california()

        page.navigate()
        // .signInAsUser(auth_creds.username, auth_creds.password)
            .waitForElementVisible('.main', 1000);

        browser.expect.element('#dashboard').to.be.present;
        browser.expect.element('#user-menu').to.be.present;
        browser.expect.element('#apps-menu').to.be.present;

        browser.saveScreenshot(screenshotsPath + 'main-page.png');
    },
    'Etape 3 : on test le menu affichant les applications': function (browser) {
        browser
        .click('#apps-menu > a')
        .click('#apps-menu > ul > li:nth-child(3) > a');

        browser
        .pause(5000)
        .saveScreenshot(screenshotsPath + 'atelier.png')
        .end();
    },
    after: function (browser) {
        browser.end();
    }
}
